$(document).ready(function() {
	
	$('#submit').click(function() {
		var wzrost = $('#wzrost').val();
		var waga = $('#waga').val();
		$('#bmi').val(null);
		var statusWagi = 'nieprawidłowe dane';
			if (wzrost > 0 && waga > 0) {

				var bmi = waga / ((wzrost/100)*(wzrost/100)); 
				$('#bmi').val(bmi);

				if (bmi >= 0 && bmi < 16) {
					statusWagi = 'wygłodzenie';
				} 
				else if (bmi >= 16 && bmi < 17) {
					statusWagi = 'wychudzenie';
				}				
				else if (bmi >= 17 && bmi < 18.5) {
					statusWagi = 'niedowaga';
				}				
				else if (bmi >= 18.5 && bmi < 25) {
					statusWagi = 'waga prawidłowa';
				}			
				else if (bmi >= 25 && bmi < 30) {
					statusWagi = 'nadwaga';
				}
				else if (bmi >= 30 && bmi < 35) {
					statusWagi = 'I stopien otyłości';
				}			
				else if (bmi >= 35 && bmi < 40) {
					statusWagi = 'II stopien otyłości';
				}					
				else if (bmi >= 40) {
					statusWagi = 'III stopień otyłości';	
				}

			}
				$('#status').val(statusWagi);
				$('#BMI').show();
				$('#Stan_Wagi').show();

		return false;
	});
	
});